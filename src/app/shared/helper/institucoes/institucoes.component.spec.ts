import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitucoesComponent } from './institucoes.component';

describe('InstitucoesComponent', () => {
  let component: InstitucoesComponent;
  let fixture: ComponentFixture<InstitucoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitucoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitucoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
