import { Component, OnInit } from '@angular/core';
import { DataApiService } from './../../service/data-api.service';


@Component({
  selector: 'app-institucoes',
  templateUrl: './institucoes.component.html',
  styleUrls: ['./institucoes.component.scss']
})
export class InstitucoesComponent implements OnInit {

  cidade: any;
  institucao: any;
  codigo: string;
  resumo: string;
  title: string;
  telefone: string;
  endereco: string;
  estado: any = [
    {edo: 'PARANÁ'},
    {edo: 'SANTA CATARINA'}
  ];

  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.codigo = 'I-01';
    // tslint:disable-next-line: max-line-length
    this.resumo = 'Entidade reconhecida pela Organização das Nações Unidas que presta auxílio aos necessitados em todo o mundo. Em Almirante Tamandaré-PR, oferece visitas domiciliares à comunidade carente, além de apoio na construção de casas para a comunidade e atividades como dança, teatro, jiu-jítsu, costura, aulas de Inglês, etc. Cerca de 200 pessoas são impactadas mensalmente.';
    this.title  = 'JOVENS COM UMA MISSÃO \"MONTE DAS ÁGUIAS\" – JOCUM';
    this.telefone = '(41) 3657-4057';
    this.endereco = 'Rodovia dos Minérios, 6099, km 16 - Jardim Dra. Belizária - Almirante Tamandaré-PR';
  }


  getCidade(estado: any) {
    const value = estado;
    console.log(value);
    this.api.getRead('cidade').subscribe(
      data => {
        const filter = data.filter((rows) => {
          return rows.estado === value;
        });
        this.cidade = filter;
      }
    );
  }

  getInstitucao(cidade: string) {
    this.api.getRead('institucoes').subscribe(
      data => {
        const filter = data.filter((rows) => {
          return rows.cidade === cidade;
        });
        this.institucao = filter;
      }
    );
  }


  selectCidade(event: any) {
    const value = event.target.value;
    this.getCidade(value);
  }
  selectInstitucao(event: any) {
    const value = event.target.value;
    this.getInstitucao(value);
  }

  selectID(event: any) {
    const value = event.target.value;
    this.api.getRead('institucoes').subscribe(data => {
      const filter = data.filter((rows) => {
        return rows.codigo === value;
      });
      this.codigo   = filter[0].codigo;
      this.resumo   = filter[0].resumo;
      this.title    = filter[0].institucao;
      this.telefone = filter[0].telefone;
      this.endereco = filter[0].endereco;
    });
  }
}
