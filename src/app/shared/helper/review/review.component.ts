import { Component, OnInit } from '@angular/core';
import { DataApiService } from './../../service/data-api.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {

  review: any;
  ano: any = [
    {ano: 2019 }, {ano: 2018 }, {ano: 2017 },
    {ano: 2016 }, {ano: 2015 }, {ano: 2014 },
    {ano: 2013 }, {ano: 2012 }, {ano: 2011 },
    {ano: 2010 }, {ano: 2009 }, {ano: 2008 },
    {ano: 2007 }
  ];
  public filterData: any = {};
  year: string;

  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.year = '2019';
    this.getData(this.year);
  }

  getData(year: string) {
    this.api.getRead('review').subscribe(
      data => {
        const dogs = data.filter((animal) => {
          return animal.ano === year;
        });
        this.review = dogs;
      }
    );
  }

  reviewYear(event) {
    const value = event.target.value;
    console.log(value);
    this.year = value;
    this.getData(value);
  }
}
