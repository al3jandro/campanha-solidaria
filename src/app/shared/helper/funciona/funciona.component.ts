import { Component, OnInit } from '@angular/core';
import { DataApiService } from './../../service/data-api.service';

@Component({
  selector: 'app-funciona',
  templateUrl: './funciona.component.html',
  styleUrls: ['./funciona.component.scss']
})
export class FuncionaComponent implements OnInit {

  funciona: any;
  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.api.getRead('data').subscribe(data => this.funciona = data);
  }

}
