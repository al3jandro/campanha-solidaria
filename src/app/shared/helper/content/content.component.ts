import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../service/data-api.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  video: any;
  videoSRC: any;

  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.api.getRead('setting').subscribe(data => {
      this.video    = data[0].video;
      this.videoSRC = data[0].videoSRC;
    });
  }

}
