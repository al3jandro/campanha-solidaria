import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  url: any = [
    { name: 'Como funciona', url: '#content', scroll: 'true' },
    { name: 'Ofertas', url: '#produtos', scroll: 'true' },
    { name: 'Edições Anteriores', url: '#review', scroll: 'true' },
    { name: 'Instituições', url: '#institucoes', scroll: 'true' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
