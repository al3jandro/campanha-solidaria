import { Component, OnInit } from '@angular/core';
import { DataApiService } from './../../service/data-api.service';
@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.scss']
})
export class OfertasComponent implements OnInit {

  products: any;
  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.getProduct();
  }

  getProduct() {
    this.api.getProducts('campanha_solidaria', 'SOL')
    .subscribe(
      data => {
        this.products = data['return'];
      }
    );
  }

}
