import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-scroll',
  templateUrl: './scroll.component.html',
  styleUrls: ['./scroll.component.scss']
})
export class ScrollComponent implements OnInit {
  windowScrolled: boolean;
// tslint:disable-next-line: deprecation
  constructor(@Inject(DOCUMENT) private document: Document) {}
  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset  > 500) {
        this.windowScrolled = true;
    } else if (this.windowScrolled && window.pageYOffset < 100) {
        this.windowScrolled = false;
    }
  }
  ngOnInit() {}
}
