import { Component, OnInit } from '@angular/core';
import { DataApiService } from './../../service/data-api.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: any;
  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.getProduct();
  }

  getProduct() {
    this.api.getProducts('dia_dos_pais', 'DDP', 8)
    .subscribe(
      data => {
        this.products = data['return'];
        console.log(this.products);
      }
    );
  }

}
