import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  url = 'https://www.condor.com.br/ws-condor/site-condor/data';
  constructor(private http: HttpClient) { }

  getProducts(category: string, modelo: string, limit: number = 100) {
    const sql = `${this.url}/produtos/${category}/${modelo}/${limit}`;
    return this.http.get(sql,
    {headers: { 'Content-Type': 'application/json; charset=utf-8', 'CONDOR-TOKEN': '9d2df4d1-bc2f-4e74-8734-fa6388a19fc4' }});
  }

  getProductsTabloide(tabloide: number, limit: number = 100) {
    const sql = `${this.url}/tabloide/${tabloide}/${limit}`;
    return this.http.get(sql,
    {headers: { 'Content-Type': 'application/json; charset=utf-8', 'CONDOR-TOKEN': '9d2df4d1-bc2f-4e74-8734-fa6388a19fc4' }});
  }

  getRandom(products: any) {
    return products[Math.floor(Math.random() * products.length)];
  }

  getRead(json: string): Observable<any> {
    return this.http.get(`./assets/json/${json}.json`);
  }

  getMessage(num: number, umo: string, dois: string) {
    Swal.fire({
      title: `Resultado Loteria Federal ${num}`,
      html:
      '<hr><div class="w-100"><div class="bgCena">' +
      '<img id="car" class="animated rubberBand bounce infinite" src="./assets/images/anime/carrinho.png" alt="">' +
      '</div></div>' +
      `<h5 class="nome-gan b-06 mt-4">${umo}</h5>` +
      `<h5 class="nome-gan b-06">${dois}</h5>`,
      width: 600,
      showCancelButton: false,
      showConfirmButton: false,
      showCloseButton: true,
    });
  }
}


