import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { DataApiService } from './../../service/data-api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.scss']
})
export class OfertaComponent implements OnInit {
  windowScrolled: boolean;
  products: any;
  // tslint:disable-next-line: deprecation
  constructor(private api: DataApiService, private spinner: NgxSpinnerService, @Inject(DOCUMENT) private document: Document) { }
  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset  > 500) {
        this.windowScrolled = true;
    } else if (this.windowScrolled && window.pageYOffset < 100) {
        this.windowScrolled = false;
    }
  }
  ngOnInit() { 
    this.getProduct();
  }

  getProduct() {
    this.spinner.show();
    this.api.getProducts('campanha_solidaria', 'SOL')
    .subscribe(
      data => {
        setTimeout(() => {
          // tslint:disable-next-line: no-string-literal
          this.products = data['return'];
          this.spinner.hide();
        }, 1500);
      }
    );
  }

}
