import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../service/data-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  gallery: any;
  products: any;
  review: any;

  constructor(
    private api: DataApiService,
    private spinner: NgxSpinnerService
    ) { }

  ngOnInit() {
    this.spinner.show();
    this.api.getRead('setting').subscribe(data => {
      setTimeout(() => {
        this.gallery  = data[0].gallery;
        this.products = data[0].products;
        this.review   = data[0].review;
        this.spinner.hide();
      }, 1500);
    });
  }
}
