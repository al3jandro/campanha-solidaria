import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgxGalleryModule } from 'ngx-gallery';
import { MomentModule } from 'ngx-moment';
import { environment } from '../environments/environment';
import { HomeComponent } from './shared/layout/home/home.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NgxSpinnerModule } from "ngx-spinner";

import { FooterComponent } from './shared/helper/footer/footer.component';
import { HeaderComponent } from './shared/helper/header/header.component';
import { MenuComponent } from './shared/helper/menu/menu.component';
import { ScrollComponent } from './shared/util/scroll/scroll.component';
import { DatePipe } from '@angular/common';
import { AnimeComponent } from './shared/util/anime/anime.component';
import { OfertasComponent } from './shared/helper/ofertas/ofertas.component';
import { MuralComponent } from './shared/helper/mural/mural.component';
import { ReviewComponent } from './shared/helper/review/review.component';
import { ContentComponent } from './shared/helper/content/content.component';
import { VideoComponent } from './shared/util/video/video.component';
import { SliderComponent } from './shared/util/slider/slider.component';
import { ScrollSpyDirective } from './shared/directive/scroll-spy.directive';
import { GalleryComponent } from './shared/util/gallery/gallery.component';
import { FilterPipe } from './shared/pipe/filter.pipe';
import { ProductsComponent } from './shared/util/products/products.component';
import { OfertaComponent } from './shared/layout/oferta/oferta.component';
import { InstitucoesComponent } from './shared/helper/institucoes/institucoes.component';
import { FuncionaComponent } from './shared/helper/funciona/funciona.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    MenuComponent,
    ScrollComponent,
    AnimeComponent,
    OfertaComponent,
    OfertasComponent,
    MuralComponent,
    ReviewComponent,
    ContentComponent,
    VideoComponent,
    SliderComponent,
    ScrollSpyDirective,
    GalleryComponent,
    FilterPipe,
    ProductsComponent,
    InstitucoesComponent,
    FuncionaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxGalleryModule,
    MomentModule,
    NgxSpinnerModule,
    ScrollToModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
