
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './shared/layout/home/home.component';
import { OfertaComponent } from './shared/layout/oferta/oferta.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'ofertas', component: OfertaComponent},
  {path: '**', redirectTo: ''}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
