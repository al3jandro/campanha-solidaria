# Landing Page MARILAN

Landing desenvolvida em Javascript usando o framework Angular em sua versão 8

## Instalação
```bash
$ git clone https://gitlab.com/amplus-condor/marilan.git
$ cd marilan
$ npm install
```
## Deploy do Angular
```bash
$ npm run build
```

## Pasta pública
```bash
$ cd dist
```
